#!/bin/bash

clear
echo ""
echo "##############################################################"
echo "# 1.) List all Developer apps to install                     #"
echo "#                                                            #"
echo "# 2.) To install all default apps                            #"
echo "#                                                            #"
echo "# 3.) Install that app of your choice                        #"
echo "#                                                            #"
echo "# 4.) Search APT                                             #"
echo "#                                                            #"
echo "# 5.) Update Computer                                        #"
echo "#                                                            #"
echo "# 6.) Exit                                                   #"
echo "##############################################################"
read -p "What choice do you want: " ask
j=1
declare -a programs=("git")



# Question 1

if [[ $ask -eq 1 ]]; then

  clear
  echo ""
  echo "Default Developer packages"
  echo "--------------------------"
  for i in "${programs[@]}"
  do
    echo "$j: $i"
    j=$((j+1))
  done
  echo ""
  read -p "Press enter to continue..."

  bash .dbp.sh
  exit 0;
fi


# Question 2

if [[ $ask -eq 2 ]]; then
  for i in "${programs[@]}"
  do
    sudo apt install $i -y
  done
  bash .dbp.sh
  exit 0;
fi






# Question 3

if [[ $ask -eq 3 ]]; then
  clear
  read -p "Type in the app you want to install (i.e. firefox chrominum git): " prog
  sudo apt install $prog -y
  bash .dbp.sh
  exit 0;
fi








# Question 4

if [[ $ask -eq 4 ]]; then
  clear
  echo ""
  read -p "Type the app name you want to search for: " prog
  sudo apt list $prog
  echo ""
  read -p "Press enter to return to Main Menu"
  bash .dbp.sh
  exit 0;

fi




# Question 5


if [[ $ask -eq 5 ]]; then
  clear
  echo "Checking for updates..."
  sleep 1
  sudo apt update && sudo apt dist-upgrade -y
  bash .dbp.sh
  exit 0;
fi









# Question 6

if [[ $ask -eq 6 ]]; then
  clear
  echo "Exiting..."
  echo ""
  exit 0;
fi
