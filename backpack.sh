#!/bin/bash

if [[ -f "/usr/bin/aurman" ]]; then
  return
else
  sudo pacman -S --noconfirm aurman
fi

clear
echo "##############################################################"
echo "# 1.) List all Developer apps to install                     #"
echo "#                                                            #"
echo "# 2.) To install all default apps                            #"
echo "#                                                            #"
echo "# 3.) Install that app of your choice                        #"
echo "#                                                            #"
echo "# 4.) Search AUR for correct filename                        #"
echo "#                                                            #"
echo "# 5.) Update Computer                                        #"
echo "#                                                            #"
echo "# 6.) Debian Based Computer Backpack (Experimental)          #"
echo "#                                                            #"
echo "# 7.) Exit                                                   #"
echo "##############################################################"
read -p "What choice do you want: " ask
j=1
declare -a programs=("atom" "discord" "git")


# Question 1

if [[ $ask -eq 1 ]]; then

  clear
  echo ""
  echo "Default dev packages"
  echo "--------------------"
  for i in "${programs[@]}"
  do
    echo "$j: $i"
    j=$((j+1))
  done
  echo ""
  read -p "Press enter to continue..."

  bash backpack.sh
  exit 0;
fi





# Question 2

if [[ $ask -eq 2 ]]; then
  for i in "${programs[@]}"
  do
    gpg --recv-keys 0fC3042E345AD05D
    aurman -S --noconfirm $i
  done
  bash backpack.sh
  exit 0;
fi











# Question 3

if [[ $ask -eq 3 ]]; then
  clear
  read -p "Type in the app you want to install (i.e. atom discord): " prog
  if [[ "$prog" == "discord" ]]; then
    gpg --recv-keys 0fC3042E345AD05D
  fi
  aurman -S --noconfirm $prog
  bash backpack.sh
  exit 0;
fi








# Question 4

if [[ $ask -eq 4 ]]; then
  clear
  echo ""
  read -p "Type the app name you want to search for: " prog
  aurman -Ss $prog
  echo ""
  read -p "Press enter to return to Main Menu"
  bash backpack.sh
  exit 0;

fi




# Question 5


if [[ $ask -eq 5 ]]; then
  clear
  echo "Checking for updates..."
  aurman -Syu --noconfirm
  bash backpack.sh
  exit 0;
fi






# Question 6

if [[ $ask -eq 6 ]]; then
  clear
  echo ""
  echo "Checking to see if apt is installed"
  echo ""
  if [[ -f "/usr/bin/apt" ]]; then
    bash .dbp.sh
  else
    sleep 1
    echo "Your not running a debian based Operating System..."
    sleep 2
    bash backpack.sh
  fi
fi










# Question 7

if [[ $ask -eq 7 ]]; then
  clear
  echo "Exiting..."
  echo ""
  exit 0;
fi
